// eslint-disable-next-line require-jsdoc
function isAuthenticated(req, res, next) {
  // Si el Boolean de autenticación devuelve true, avanzamos al siguiente punto
  if (req.isAuthenticated()) {
    return next();
  } else {
    // En caso de no estar logueados, redireccionamos a login
    return res.sendStatus(401);
  }
}

module.exports = {
  isAuthenticated,
};
