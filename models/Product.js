const mongoose = require('mongoose');

const Schema = mongoose.Schema;

// Creamos el esquema de producto
const ProductSchema = new Schema(
  {
    name: {type: String, required: true},
    price: {type: Number, required: true, min: 0},
    description: {type: String, required: true},
    image: {type: String, required: true},
  },
  {
    timestamps: true,
  },
);

// Creamos y exportamos el modelo producto
const Product = mongoose.model('Product', ProductSchema);
module.exports = Product;
