const mongoose = require('mongoose');

const Schema = mongoose.Schema;

// Creamos el esquema de la compra
const PurchaseSchema = new Schema(
  {
    cart: [{type: mongoose.Types.ObjectId, ref: 'Product'}],
    address: {type: mongoose.Types.ObjectId, ref: 'Address'},
    totalPrice: {type: Number},
  },
  {
    timestamps: true,
  },
);

// Creamos y exportamos el modelo compra
const Purchase = mongoose.model('Purchase', PurchaseSchema);
module.exports = Purchase;
