const mongoose = require('mongoose');

const Schema = mongoose.Schema;

// Creamos el esquema de usuarios
const UserSchema = new Schema(
  {
    name: {type: String, required: true},
    surname: {type: String, required: true},
    username: {type: String, required: true},
    email: {type: String, required: true},
    password: {type: String, required: true},
    image: {type: String, required: false},
    cart: [{type: mongoose.Types.ObjectId, ref: 'Product'}],
  },
  {
    timestamps: true,
  },
);

// Creamos y exportamos el modelo User
const User = mongoose.model('User', UserSchema);
module.exports = User;
