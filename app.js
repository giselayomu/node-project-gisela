require('dotenv').config();

const express = require('express');
const session = require('express-session');
const MongoStore = require('connect-mongo');
const passport = require('passport');
const userRouter = require('./routes/user.routes');
const productRouter = require('./routes/product.routes');
require('./passport'); // Requerimos nuestro archivo de configuración

const app = express();
require('./config/db');

app.use(express.json());
app.use(express.urlencoded({extended: false}));


app.use(
  session({
    // ¡Este secreto tendremos que cambiarlo en producción!
    secret: process.env.SESSION_SECRET || 'upgradehub_node',
    // Solo guardará la sesión si hay cambios en ella.
    resave: false,
    // Lo usaremos como false debido a que
    // gestionamos nuestra sesión con Passport
    saveUninitialized: false,
    cookie: {
      // Milisegundos de duración de nuestra cookie,
      // en este caso será una hora.
      maxAge: 3600000,
    },
    store: MongoStore.create({
      mongoUrl: process.env.DB_URI || 'mongodb://localhost:27017/node-proyecto',
    }),
  }),
);

// Añadimos el nuevo middleware al servidor
app.use(passport.initialize());
app.use(passport.session()); // Este middlware añadirá
// sesiones a nuestros usuarios

app.use('/user', userRouter);
app.use('/product', productRouter);

const PORT = process.env.PORT || 3000;

app.listen(PORT, () => {
  console.log(`Listening in http://localhost:${PORT}`);
});
