module.exports = {
  'env': {
    'browser': true,
    'commonjs': true,
    'es2021': true,
  },
  'extends': ['prettier', 'google'],
  'plugins': ['prettier'],
  'parserOptions': {
    'ecmaVersion': 12,
  },
  'rules': {
    'indent': [2, 2],
  },
};
