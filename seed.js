require('dotenv').config();
const mongoose = require('mongoose');
const User = require('./models/User');
const Product = require('./models/Product');
const bcrypt = require('bcrypt');

const users = [
  {
    name: 'gisela',
    surname: 'García',
    username: 'giselagarcia',
    email: 'giselagarcia@email.com',
    password: '123gisela',
    image:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTLDgDTiDh9GbUWFSfklDZKDcLjbhIZ1k3Dow&usqp=CAU',
    cart: [],
  },
  {
    name: 'Martin',
    surname: 'Fernández',
    username: 'martinfernandez',
    email: 'martin@email.com',
    password: '123martin',
    image:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTLDgDTiDh9GbUWFSfklDZKDcLjbhIZ1k3Dow&usqp=CAU',
    cart: [],
  },
  {
    name: 'Dolores',
    surname: 'Fuertes',
    username: 'doloresfuertes',
    email: 'fuertes@email.com',
    password: '123dolores',
    image:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTLDgDTiDh9GbUWFSfklDZKDcLjbhIZ1k3Dow&usqp=CAU',
    cart: [],
  },
  {
    name: 'Toni',
    surname: 'Romero',
    username: 'toniromero',
    email: 'romero@email.com',
    password: '123romero',
    image:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTLDgDTiDh9GbUWFSfklDZKDcLjbhIZ1k3Dow&usqp=CAU',
    cart: [],
  },
  {
    name: 'Fernando',
    surname: 'Fernández',
    username: 'fernandofernandez',
    email: 'ferfera@email.com',
    password: '123fernando',
    image:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTLDgDTiDh9GbUWFSfklDZKDcLjbhIZ1k3Dow&usqp=CAU',
    cart: [],
  },
  {
    name: 'Minombreotravez',
    surname: 'García',
    username: 'minombreotravez',
    email: 'minombregarcia@email.com',
    password: '123minombre',
    image:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTLDgDTiDh9GbUWFSfklDZKDcLjbhIZ1k3Dow&usqp=CAU',
    cart: [],
  },
  {
    name: 'Otronombre',
    surname: 'Apellido2',
    username: 'otronombre',
    email: 'nombre@email.com',
    password: '123nombre',
    image:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTLDgDTiDh9GbUWFSfklDZKDcLjbhIZ1k3Dow&usqp=CAU',
    cart: [],
  },
  {
    name: 'Nombre2',
    surname: 'Apellido2',
    username: 'nombreapellido2',
    email: 'nombreapellido2@email.com',
    password: '123nombre2',
    image:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTLDgDTiDh9GbUWFSfklDZKDcLjbhIZ1k3Dow&usqp=CAU',
    cart: [],
  },
  {
    name: 'Nombre3',
    surname: 'Apellido3',
    username: 'nombreapellido3',
    email: 'nombreapellido3@email.com',
    password: '123nombre3',
    image:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTLDgDTiDh9GbUWFSfklDZKDcLjbhIZ1k3Dow&usqp=CAU',
    cart: [],
  },
  {
    name: 'Nombre4',
    surname: 'Apellido4',
    username: 'nombreapellido4',
    email: 'nombreapellido4@email.com',
    password: '123nombre4',
    image:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTLDgDTiDh9GbUWFSfklDZKDcLjbhIZ1k3Dow&usqp=CAU',
    cart: [],
  },
  {
    name: 'Nombre5',
    surname: 'Apellido5',
    username: 'nombreapellido5',
    email: 'nombreapellido5@email.com',
    password: '123nombre5',
    image:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTLDgDTiDh9GbUWFSfklDZKDcLjbhIZ1k3Dow&usqp=CAU',
    cart: [],
  },
  {
    name: 'Nombre6',
    surname: 'Apellido6',
    username: 'nombreapellido6',
    email: 'nombreapellido6@email.com',
    password: '123nombre6',
    image:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTLDgDTiDh9GbUWFSfklDZKDcLjbhIZ1k3Dow&usqp=CAU',
    cart: [],
  },
  {
    name: 'Nombre7',
    surname: 'Apellido7',
    username: 'nombreapellido7',
    email: 'nombreapellido7@email.com',
    password: '123nombre7',
    image:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTLDgDTiDh9GbUWFSfklDZKDcLjbhIZ1k3Dow&usqp=CAU',
    cart: [],
  },
  {
    name: 'Nombre8',
    surname: 'Apellido8',
    username: 'nombreapellido8',
    email: 'nombreapellido8@email.com',
    password: '123nombre8',
    image:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTLDgDTiDh9GbUWFSfklDZKDcLjbhIZ1k3Dow&usqp=CAU',
    cart: [],
  },
  {
    name: 'Nombre9',
    surname: 'Apellido9',
    username: 'nombreapellido9',
    email: 'nombreapellido9@email.com',
    password: '123nombre9',
    image:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTLDgDTiDh9GbUWFSfklDZKDcLjbhIZ1k3Dow&usqp=CAU',
    cart: [],
  },
  {
    name: 'Nombre10',
    surname: 'Apellido10',
    username: 'nombreapellido10',
    email: 'nombreapellido10@email.com',
    password: '123nombre10',
    image:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTLDgDTiDh9GbUWFSfklDZKDcLjbhIZ1k3Dow&usqp=CAU',
    cart: [],
  },
  {
    name: 'Nombre11',
    surname: 'Apellido11',
    username: 'nombreapellido11',
    email: 'nombreapellido11@email.com',
    password: '123nombre11',
    image:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTLDgDTiDh9GbUWFSfklDZKDcLjbhIZ1k3Dow&usqp=CAU',
    cart: [],
  },
  {
    name: 'Nombre12',
    surname: 'Apellido12',
    username: 'nombreapellido12',
    email: 'nombreapellido12@email.com',
    password: '123nombre12',
    image:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTLDgDTiDh9GbUWFSfklDZKDcLjbhIZ1k3Dow&usqp=CAU',
    cart: [],
  },
  {
    name: 'Nombre13',
    surname: 'Apellido13',
    username: 'nombreapellido13',
    email: 'nombreapellido13@email.com',
    password: '123nombre13',
    image:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTLDgDTiDh9GbUWFSfklDZKDcLjbhIZ1k3Dow&usqp=CAU',
    cart: [],
  },
  {
    name: 'Nombre14',
    surname: 'Apellido14',
    username: 'nombreapellido14',
    email: 'nombreapellido14@email.com',
    password: '123nombre14',
    image:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTLDgDTiDh9GbUWFSfklDZKDcLjbhIZ1k3Dow&usqp=CAU',
    cart: [],
  },

];

const products = [
  {
    name: 'Alfombra olfateo para perros',
    price: 20,
    // eslint-disable-next-line max-len
    description: 'La alfombra de olfateo para perros es un juguete sencillo pero muy innovador con el que pondrás a prueba la destreza de tu peludo.',
    image: 'https://images1.tiendanimal.es/240/17208-alfombra-olfateo-para-perros.jpg',
  },
  {
    name: 'Juego interactivo Solitario para perros',
    price: 18,
    // eslint-disable-next-line max-len
    description: 'Dog Activity Solitaire, es un juego de inteligencia para perros de nivel 1.',
    image: 'https://images1.tiendanimal.es/240/20616-IMG00012705.jpg',
  },
  {
    name: 'Kong Wobbler Dispensador de comida para perros',
    price: 11,
    // eslint-disable-next-line max-len
    description: 'Este producto, en apariencia, es solo un dispensador de comida y golosinas para perros , pero también se trata de un juguete para ellos.',
    image: 'https://images1.tiendanimal.es/240/5787_kong_wobbler_dispensador_comida_perrosA.jpg',
  },
  {
    name: 'Kong para cachorros "Puppy Kong"',
    price: 8,
    // eslint-disable-next-line max-len
    description: 'Se trata de un juguete pensado para perros de dos a nueve meses y se convertirá en el amigo favorito de tu mascota.',
    image: 'https://images1.tiendanimal.es/240/1183-kong-puppy-juguete-cachorros.jpg',
  },
  {
    name: 'Mordedor para perros TK-Pet SportDog neumático con cuerda',
    price: 8,
    // eslint-disable-next-line max-len
    description: 'El mordedor para perros TK-Pet SportDog es juguete con forma de neumático con cuerda que está diseñado especialmente para que disfrutes practicando actividades al aire libre con tu mascota.',
    image: 'https://images1.tiendanimal.es/150/12239-mordedor-para-perros-tkpet-sportdog-neumatico-cuer-mta-12239.jpg',
  },
];

const userDocuments = users.map((user) => {
  const hash = bcrypt.hashSync(user.password, 10);
  const a = {
    ...user,
    password: hash,
  };
  return new User(a);
});
const productDocuments = products.map((product) => new Product(product));
console.log(process.env.DB_URI || 'mongodb://localhost:27017/node-proyecto');
mongoose
  .connect(process.env.DB_URI || 'mongodb://localhost:27017/node-proyecto', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(async () => {
    const allUsers = await User.find();
    if (allUsers.length) {
      await User.collection.drop();
    }
  })
  .catch((err) => console.log(`Error deleting data: ${err}`))
  .then(async () => {
    await User.insertMany(userDocuments);
  })
  .then(async () => {
    const allProducts = await Product.find();
    if (allProducts.length) {
      await Product.collection.drop();
    }
  })
  .catch((err) => console.log(`Error deleting data: ${err}`))
  .then(async () => {
    await Product.insertMany(productDocuments);
  })
  .catch((err) => console.log(`Error creating data: ${err}`))
  .finally(() => mongoose.disconnect());
