const express = require('express');
const passport = require('passport');
const authMiddleware = require('../middlewares/auth.middleware');
const fileMiddlewares = require('../middlewares/file.middleware');
const User = require('../models/User.js');
// eslint-disable-next-line new-cap
const router = express.Router();

router.post(
  '/register',
  // eslint-disable-next-line max-len
  [fileMiddlewares.upload.single('picture'), fileMiddlewares.uploadToCloudinary],
  (req, res, next) => {
    passport.authenticate('register', (error, user) => {
      if (error) {
        return res.status(401).json(error);
      }
      req.logIn(user, (err) => {
        // Si hay un error logeando al usuario, resolvemos el controlador
        if (err) {
          return res.status(401).json(err);
        }

        // Si no hay error, redijimos a los usuarios a la ruta que queramos
        return res.json(user);
      });
    })(req, res, next);
  },
);

router.post('/login', (req, res, next) => {
  passport.authenticate('login', (error, user) => {
    if (error) {
      return res.status(401).json(error);
    }

    req.logIn(user, (err) => {
      // Si hay un error logeando al usuario, resolvemos el controlador
      if (err) {
        return res.send(401).json(err);
      }

      // Si no hay error, redijimos a los usuarios a la ruta que queramos
      return res.status(200).json(user);
    });
  })(req, res, next);
});

// Get user - el usuario quiere ver su perfil
router.get('/', [authMiddleware.isAuthenticated], async (req, res) => {
  const id = req.user.id;
  try {
    const user = await User.findOne(
      {_id: id},
      {name: 1, surname: 1, email: 1, cart: 1},
    ).populate('cart');
    if (user) {
      return res.status(200).json(user);
    } else {
      return res.status(404).json('No user found by this id');
    }
  } catch (err) {
    return res.status(500).json(err);
  }
});

router.get('/logout', [authMiddleware.isAuthenticated], (req, res, next) => {
  // Destruimos el objeto req.user para este usuario
  req.logout();
  req.session.destroy(() => {
    // Eliminamos la cookie de sesión al cancelar la sesión
    res.clearCookie('connect.sid');
    // Redirijimos el usuario a la home
    return res.sendStatus(204);
  });
});

// Edición de perfil
router.put('/', [authMiddleware.isAuthenticated], async (req, res, next) => {
  try {
    const id = req.user.id;
    const updatedUser = await User.findByIdAndUpdate(
      id, // La id para encontrar el documento a actualizar
      {username: req.body.username}, // Campos que actualizaremos
      {email: req.body.email},
      {password: req.body.password},
      {new: true}, // Usando esta opción, conseguiremos el documento
      // actualizado cuando se complete el update
    );
    return res.status(200).json(updatedUser);
  } catch (err) {
    next(err);
  }
});

// Darse de baja
router.delete('/', [authMiddleware.isAuthenticated], async (req, res, next) => {
  try {
    const id = req.user.id;
    // Borramos el usuario
    await User.findByIdAndDelete(id);
    req.logout();
    req.session.destroy(() => {
      // Eliminamos la cookie de sesión al cancelar la sesión
      res.clearCookie('connect.sid');
      // Redirijimos el usuario a la homes
      return res.status(200).json('You have been deleted!');
    });
  } catch (err) {
    next(err);
  }
});

module.exports = router;
