const express = require('express');

const User = require('../models/User');
// eslint-disable-next-line new-cap
const router = express.Router();

// Mostrar carrito
router.get('/cart', [authMiddleware.isAuthenticated], async (req, res) => {
  const id = req.user.id;
  try {
    // eslint-disable-next-line max-len
    const user = await User.findOne({_id: id}, {cart: 1}).populate('cart');
    return res.status(200).json(user.cart);
  } catch (err) {
    return res.status(500).json(err);
  }
});

// añadir producto al carrito
router.post('/add/:id', [authMiddleware.isAuthenticated], async (req, res) => {
  const id = req.user.id;
  try {
    // eslint-disable-next-line max-len
    const user = await User.findOne({_id: id}, {cart: 1});
    user.cart.push(req.params.id);
    const updatedCart = await User.findByIdAndUpdate(
      id, // La id para encontrar el documento a actualizar
      {cart: user.cart}, // Campos que actualizaremos
      {new: true}, // Usando esta opción, conseguiremos el documento
      // actualizado cuando se complete el update
    );
    return res.status(200).json(updatedCart);
  } catch (err) {
    return res.status(500).json(err);
  }
});

module.exports = router;
