const express = require('express');
const Product = require('../models/Product');

// eslint-disable-next-line new-cap
const router = express.Router();

router.get('/', async (req, res) => {
  try {
    const products = await Product.find({}, {name: true, price: true});
    return res.status(200).json(products);
  } catch (err) {
    return res.status(500).json(err);
  }
});

router.get('/:id', async (req, res) => {
  const id = req.params.id;
  try {
    const products = await Product.findOne({_id: id});
    return res.status(200).json(products);
  } catch (err) {
    return res.status(500).json(err);
  }
});

router.get('/by-name/:name', async (req, res) => {
  const name = req.params.name;
  try {
    const products = await Product.find(
      {name: name},
      {name: true, price: true},
    );
    return res.status(200).json(products);
  } catch (err) {
    return res.status(500).json(err);
  }
});

router.get('/by-price/:price', async (req, res) => {
  const price = req.params.price;
  try {
    const products = await Product.find(
      {price: price},
      {name: true, price: true},
    );
    return res.status(200).json(products);
  } catch (err) {
    return res.status(500).json(err);
  }
});


module.exports = router;
